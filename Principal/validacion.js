document.querySelector('#sesionini').addEventListener('click', 
iniciarSesion);

function iniciarSesion(){
    var sCorreo = '';
    var sContrasenna = '';
    var bAcceso = false;
    
    sCorreo = document.querySelector('#correo').value;
    sContrasenna = document.querySelector('#contrasena').value;

    bAcceso = validarCredenciales(sCorreo, sContrasenna);
    if (bAcceso == true) {
        ingresar();
    }
}
